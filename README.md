
# poc-ruby
POC scripts and other testing scripts written in ruby
## aws_logs.rb
A quick script that lists the 'log_' prefix files to the screen (date, key, size) in order to see if logs have been updated.
## changemonitor.rb
'changemonitor.rb' looks for file changes based on the file pattern (mtime of the file and alerts file changes). Can be used for unwanted file changes in certain folders etc or in a reversed way (e.g. to check if log file is not being appended for a while)
## fake_cert.rb
'fake_cert.rb' overwrites the public key in the defined certificate. The script generates NIST p384 key pair (because Estonian National ID uses the same algorithm), the generated fake certificate can be used to validate, if server side validates the client certificate correctly. 
## fsaudit.rb
A prototype for understanding file system events in windows event logs. It uses JSON-format windows events as an input and correlates 4624, 4634, 4663, 4658, 4659 and 4660 events to understand READ, WRITE, RENAME and DELETE events on a shared file system. 
## generate_asice.rb
A simple prototype script for generating ASICE containers from scratch.
Usage: `ruby generate_asice.rb generated.asice file1.pdf file2.txt file3.jpg`
## get_azure_audit_log.rb
A prototype to fetch azure logs via light-weight ruby script (without any API extra-weight)
## progress.rb
A totally useless program that just shows you a progress of something in a really fancy way (using ANSI escape codes)
## nonblock.rb
'nonblock.rb' enables me to interact with already running processes, e.g. manipulate dictionary content of my running scripts
## spectx_dot_converter.rb
'spectx_dot_converter.rb' finds your spectx scripts from a prededined glob and scans the scripts for lines including the commands written in dot-notation (soon to-be-deprecated). Script replaces the commands with pipe-notation. 
## sudoku_solver.rb
'sudoku_solver.rb' helps you in solving SUDOKU puzzles. 
## syn_banner.rb
'syn_banner.rb' detects potential OS (or atleast fingerprints the SYN packet) to some extent. Just use TCPDUMP's output to do the magic: 
From live: `tcpdump -U -i en0 -nn -w - "tcp[tcpflags] & (tcp-syn) != 0" | ruby syn_banner.rb`
From pcap: `tcpdump -U -nn -w - "tcp[tcpflags] & (tcp-syn) != 0" -r dump.pcap | ruby syn_banner.rb`
## xtail.rb
'tail -f' functionality implemented in ruby that enables you to tail multiple files simultaniously based on a file pattern. Script scans the changes on your disk based on the pattern and adds (or removes) the respective files. In other words - the files you want to monitor might not exist yet.
