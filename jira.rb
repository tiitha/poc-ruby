#!/usr/bin/env ruby
require 'io/console'
require 'net/http'
require 'net/https'
require 'uri'
require 'optparse'
require 'json'
require 'date'
require 'optparse'

def _get(url)
	uri = URI.parse(url)
	https = Net::HTTP.new(uri.host,uri.port)

    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Get.new("#{uri.path}?#{uri.query}", initheader = { 'Content-Type' => 'application/json'})
	req['Authorization'] = "Bearer #{$opts['jira_token']}"

	https.request(req)
end

def _logged_in(sess)
	url = "#{$opts['jira_uri']}/rest/api/2/issue/#{$opts['jira_project']}-1"
	uri = URI.parse(url)

	proxy = Net::HTTP::Proxy("localhost", "8080")
	https = proxy.start(uri.host, :use_ssl => true, :verify_mode => OpenSSL::SSL::VERIFY_NONE)	

    req = Net::HTTP::Get.new("#{uri.path}?#{uri.query}", initheader = { 'Content-Type' => 'application/json'})
	req['Authorization'] = "Bearer #{$opts['jira_token']}"

	res = https.request(req)
	puts res.code
	exit
end

def _login()

	url = "#{$opts['jira_uri']}/rest/auth/1/session"
	uri = URI.parse(url)

    https = Net::HTTP.new(uri.host,uri.port)
	https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Post.new("#{uri.path}", initheader = { 'Content-Type' => 'application/json'})

	data = {
		"username" => $opts['jira_user'],
		"password" => $opts['jira_pass']
	}

	req.body = data.to_json
    res = https.request(req)
	sess = nil
	if res.code == "200"
		sess = JSON.parse(res.body)['session']['value'] # JSESSIONID
		File.open($opts['jira_sess_file'], "w") { |f| f.write sess }
	end
	sess
end

def update_current_week(key, desc)

	url = "#{$opts['jira_uri']}/rest/api/2/issue/#{key}"
	data = { "fields" => { "description" => desc }}.to_json
	
	uri = URI.parse(url)
	https = Net::HTTP.new(uri.host,uri.port)

    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

	req = Net::HTTP::Put.new("#{uri.path}?#{uri.query}", initheader = { 'Content-Type' => 'application/json'})
	req['Authorization'] = "Bearer #{$opts['jira_token']}"
	req.body = data

	response = https.request(req)
	puts "Task update error!" unless response.code == "204"
end

def fetch_week(summary)
	url = "#{$opts['jira_uri']}/rest/api/2/search?&maxResults=10&jql=project=#{$opts['jira_project']}%20AND%20summary~#{summary}"
	res = _get(url)
	
	nil unless res.code == "200"
	JSON.parse(res.body)
end

def mark_week_done(key)
	puts "Marking #{key} as 'Done'"
	res = update_week_status(key, "41")
	puts "Marked DONE!" if res.code == "204"
end

def mark_week_in_progress(key)
	puts "Marking #{key} as 'In Progress'"
	res = update_week_status(key, "11")
	puts "Issue set to 'In Progress' state" if res.code == "204"
end


def update_week_status(key, transition)
	url = "#{$opts['jira_uri']}/rest/api/2/issue/#{key}/transitions" # ?expand=transitions.fields"
	uri = URI.parse(url)

    https = Net::HTTP.new(uri.host,uri.port)
	https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

    req = Net::HTTP::Post.new("#{uri.path}", initheader = { 'Content-Type' => 'application/json'})
	req['Authorization'] = "Bearer #{$opts['jira_token']}"

	data = {
		"transition" => {
			"id" => transition
		}
	}

	req.body = data.to_json
    https.request(req)
end

def fetch_last_week()
	t = Time.new - 7 * 86400 # 1 week ago
	week = fetch_week( "week-#{t.strftime("%Y-%W")}-#{$opts['jira_user']}" )

	if week['total'] > 0
		key = week['issues'][0]['key']
		desc = week['issues'][0]['fields']['description']
		done,todo = desc.split("\r\n\r\n")
		return [key, todo]
	end
	return nil
end

def create_week(summary, todo)

	todo = "TODO:" if todo == ""
	# create new week task
	data = {
		"fields" => {
		   "project" => { "key" => $opts['jira_project'] },
		   "summary" => summary,
		   "description" => "DONE:\r\n\r\n#{todo}\r\n",
		   "issuetype" => { "name" => "Task" }
		}
	}

	url = "#{$opts['jira_uri']}/rest/api/2/issue"
	uri = URI.parse(url)

    https = Net::HTTP.new(uri.host,uri.port)
	https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE


    req = Net::HTTP::Post.new("#{uri.path}", initheader = { 'Content-Type' => 'application/json'})
	req['Authorization'] = "Bearer #{$opts['jira_token']}"

	req.body = data.to_json

    res = https.request(req)

	nil unless res.code == "200"
	issue = JSON.parse(res.body)
	mark_week_in_progress(issue['key'])
	issue 
end

$opts = {
	'jira_uri' => 'https://dev.lhv.eu/jira',
	'jira_project' => 'SEC',
	'jira_user' => 'tiith',
	'jira_token' => ''
}

options = Hash.new{|h,k|h[k]=[]}

o = OptionParser.new do |opts|
	opts.on('-d', '--done=MSG', 'Add a completed task') do |msg|
		msg[0] = msg[0].upcase
		options[:done] << msg
	end

	opts.on('-t', '--todo=MSG', 'Add a task reminder') do |msg|
		msg[0] = msg[0].upcase
		options[:todo] << msg
	end

	opts.on('-w', '--week', 'Get tasks from last week') do |msg|
		options[:week] << 1
	end
  
end

o.parse(ARGV)

t = Time.new - if options[:week].size == 1
	7 * 86400 # 1 week ago
else
	0
end

summary = "week-#{t.strftime("%Y-%W")}-#{$opts['jira_user']}"
week = fetch_week(summary)

exit if week.nil?
if week['total'] == 0 && options[:week].size == 0
	# existing week is missing. Deal w. last weeks tasks and create a new issue (except when looking at last week)
	todo = ""
	last_week = fetch_last_week()

	unless last_week.nil?
		key,todo = last_week
		mark_week_done(key)
	end
	
	week = fetch_week(summary) unless create_week(summary, todo).nil? 
	exit if week.nil?
end

key = week['issues'][0]['key']
desc = week['issues'][0]['fields']['description']

done,todo = desc.split("\r\n\r\n")

done += "\r\n * #{options[:done].join("\r\n * ")}" if options[:done].size > 0
todo += "\r\n * #{options[:todo].join("\r\n * ")}" if options[:todo].size > 0

todo = "" if options[:week].size == 1 # don't show last weeks' TODO
desc = "#{done}\r\n\r\n#{todo}"

update_current_week(key, desc) if options[:done].size > 0 or options[:todo].size > 0

puts "\nISSUE: #{key}"
puts "\n#{desc}"