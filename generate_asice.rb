require 'zip'
require 'builder'

def create_xml_manifest(files) 
    raw = ""
    x = Builder::XmlMarkup.new(:target => raw, :indent => 1)

    x.instruct!
    x.tag!('manifest:manifest', {"xmlns:manifest"=>"urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"}) do 

        # manifest entry
        x.tag!('manifest:file-entry', {
            "manifest:media-type" => "application/vnd.etsi.asic-e+zip",
            "manifest:full-path" => "/"
        })

        # add file entries
        files.each do |f|
            begin
                # unix command for file-type
                type = `file -Ib #{f}`.gsub(/\n/,"") 
                type = type.split(";")[0] if type.include? ";"
            rescue
                # if for some reason the command failed
                type = "application/octet-stream"
            end

            x.tag!('manifest:file-entry', {
                "manifest:media-type" =>  type,
                "manifest:full-path" => f
            })

        end
    end
    raw
end

if ARGV.size() < 2
  printf("  Usage: ./%1$s out_file file [file ...] \nExample: ./%1$s container.asice document.txt\n\n", File.basename(__FILE__))
  exit(1)
end

output = ARGV[0]
files = ARGV[1..ARGV.size()]


Zip::OutputStream.open(output) do |zip|
    # create mimetype entry
    zip.put_next_entry("mimetype", "", "", Zip::Entry::STORED, 0)
    zip.puts "application/vnd.etsi.asic-e+zip"

    # create manifest entry
    zip.put_next_entry("META-INF/manifest.xml", "", "", Zip::Entry::STORED, 0)
    zip.puts create_xml_manifest(files)

    files.each do |fn|
        zip.put_next_entry(fn, "", "", Zip::Entry::STORED, 0)
        zip.puts File.open(fn).read()
    end
end
