#!/usr/bin/env ruby
require 'fileutils'

def find_files(pattern)
	files = []
	Dir.glob(pattern).each do |fn|
		files << fn
	end
	return files
end

def dot_to_pipe(line)
  commands = line.scan($command_regex)  
  commands.each do |c|
    line.sub! ".#{c[0]}(", " | #{c[0]}("
  end
  return line
end

$command_regex = /\.([a-z]+)\(/

file_pattern = "/data/spectx_scripts/*/*/*.sx"

files = find_files(file_pattern)

files.each() do |file|
	content = ""
	changed = false
	File.open(file, "r") do |f|
		f.each_line do |line|
			if line.include? "."
        line = dot_to_pipe(line)
				changed = true
			end
			content << line
		end
	end

	if changed == true
		print "Updated file '%s'\n" % file
		File.open(file, "w") do |f|
			f.puts content
		end
	end
end