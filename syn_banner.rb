#!/usr/bin/env ruby
require 'pcap'
#
# USAGE:: 
# 1. tcpdump -U -i en0 -nn -w - "tcp[tcpflags] & (tcp-syn) != 0"| ruby syn_dump.rb 
# 2. tcpdump -U -nn -w - -r dump.pcap | ruby syn_dump.rb 
#
# https://www.infoq.com/articles/tcp-syn-security-unauthorized-os/
#

$os = {}
$results = {}
$last_height = 0

config = {
	'Windows 95' 		=> { ttl:  32, length: 44, window: 8192 },
	'Windows 98' 		=> { ttl: 128, length: 48, window: 8192 },
	'Windows 2000' 		=> { ttl: 128, length: 48, window: 16384 },
	'Windows XP' 		=> { ttl: 128, length: 48, window: 64240 },
	'Windows Vista/7/8' => { ttl: 128, length: 52, window: 8192 },
	'Linux 2.4/2.6' 	=> { ttl:  64, length: 60, window: 5840 },
	'Google Linux' 		=> { ttl:  64, length: 60, window: 5720 },
	'Linux kernel 2.2' 	=> { ttl:  64, length: 60, window: 32120 },
	'FreeBSD / Mac' 	=> { ttl:  64, length: 64, window: 65535 },
	'OpenBSD' 			=> { ttl:  64, length: 64, window: 16384 },
	'AIX 4.3' 			=> { ttl:  64, length: 44, window: 16384 },
	'Cisco IOS 12.4' 	=> { ttl: 255, length: 44, window: 4128 },
	'Solaris 7' 		=> { ttl: 255, length: 44, window: 8760 }
}

def get_fingerprint(ttl: , length:, window:)
	# since we don't have tuples ...
	return (window << 24) + (length << 8) + ttl
end

def syn_magic(pkt)
	# create syn fingerprint
	fingerprint = get_fingerprint(ttl: pkt.ip_ttl, length: pkt.ip_len, window: pkt.tcp_win)

	$results[fingerprint] = 0 unless $results.include? fingerprint
	$results[fingerprint] += 1		

	os = "N/A"
	os = $os[fingerprint] if $os.include? fingerprint

	update_screen()
#	puts "\033[1;33;49m#{os}\033[0;39;49m\tTTL: #{pkt.ip_ttl}, Window size: #{pkt.tcp_win}, Packet size: #{pkt.ip_len}, fingerprint: #{fingerprint}"

end

def update_screen()
	# move up to the starting position
	puts "\033[#{$last_height+1}A"

	$results.each do |fp, count|
		if $os.include? fp
			os = $os[fp]
		else
			os = "TTL: #{fp & 255}, PktLen: #{(fp >> 8) & 65535}, Window: #{(fp >> 24) & 65535}"

		end
		puts "\033[K#{'%-8.8s' % count}\t \033[1;33;49m#{os}\033[0;39;49m"		
	end
	$last_height = $results.size
end

config.each do |banner, conf|	
	fingerprint = get_fingerprint(ttl: conf[:ttl], length: conf[:length], window: conf[:window])
	$os[fingerprint] = banner
end


puts "\nSYN count\t OS"

cap = Pcap::Capture.open_offline("-")

cap.loop(0) do |pkt|
	if pkt.tcp?
		syn_magic(pkt) if pkt.tcp_syn? and not pkt.tcp_ack?
	end
end	

cap.close



