#!/usr/bin/env ruby
prop = ['-','\\','|','/']
step = 1

a = 0
loop do
	a += step
	puts "["+prop[ (a / step) % 4 ]+"] Status: \033[1;49m " + a.to_s.rjust(2) + "% \033[0;49m "
	puts "\033[2A"
	break if a == 100
	sleep(0.1)
end
puts "[v] Status: Done\033[K"