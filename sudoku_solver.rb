#!/usr/bin/ruby

# prints the sudoku board
def display_board(board)
	puts ""
	for y in 0 .. board.size() -1
		print(" - - - - - - - - - - - - - - \n") if y % 3 == 0 && y > 0
		for x in 0 .. board[y].size() - 1
			print " #{board[y][x]} "
			print "|" if x % 3 == 2 && x < board.size() -1 
		end

		print "\n"
	end
	puts ""
end

# finds the next empty square on the sudoku board
def find_empty(board)
	x = y = nil
	for y in 0 .. board.size() - 1
		for x in 0 .. board[y].size() -1
			return [x,y] if board[y][x] == 0
		end
	end
	return [nil, nil]
end

# checks if the number is suitable for the position
def valid(board, y, x, nr)

	# box 
	box_y = y/3.floor * 3
	box_x = x/3.floor * 3

	for i in box_y .. box_y + 2
		for j in box_x .. box_x + 2
			return false if board[i][j] == nr && i != y && j != x
		end
	end

	# row 
	for i in 0..board[y].size() - 1
		return false if board[y][i] == nr
	end

	# cols
	for i in 0..board.size() - 1
		return false if board[i][x] == nr
	end

	return true
end

# solve wrapper
def solve(board)
	loop do 
		x,y = find_empty(board)
		break if x.nil?

		for i in 1..9
			if valid(board, y, x, i)
				board[y][x] = i
				return true if solve(board)
				board[y][x] = 0
			end
		end
		return false
	end
	display_board(board)
end


board = [
    [7,8,0,4,0,0,1,2,0],
    [6,0,0,0,7,5,0,0,9],
    [0,0,0,6,0,1,0,7,8],
    [0,0,7,0,4,0,2,6,0],
    [0,0,1,0,5,0,9,3,0],
    [9,0,4,0,6,0,0,0,5],
    [0,7,0,3,0,0,0,1,2],
    [1,2,0,0,0,7,4,0,0],
    [0,4,9,2,0,6,0,0,7]
]

solve(board)

