#!/usr/bin/env ruby
require 'digest'
require 'net/http'
require 'net/https'
require 'uri'
require 'nokogiri'

options = {
	service: 's3',
	max_keys: 10000,
	prefix: 'log_',
	bucket: 'BUCKET_NAME',
	region: 'REGION_NAME',
	access_key: 'ACCESS_KEY', 
	secret_access_key: 'SECRET_ACCESS_KEY'
}

def getSignatureKey(key, datestamp, region_name, service_name)
    kDate 	= OpenSSL::HMAC.digest('sha256', "AWS4" + key, datestamp)
    kRegion 	= OpenSSL::HMAC.digest('sha256', kDate, region_name)
    kService	= OpenSSL::HMAC.digest('sha256', kRegion, service_name)
    kSigning	= OpenSSL::HMAC.digest('sha256', kService, "aws4_request")
    return kSigning
end

curr_utc = Time.now.getutc
curr_date = curr_utc.strftime("%Y%m%d")
curr_time = curr_utc.strftime("%Y%m%dT%H%M%SZ")

req_sha		= Digest::SHA256.hexdigest("")

uri = sprintf("https://%s.%s.%s.amazonaws.com/?encoding-type=url&max-keys=%s&prefix=%s", 
	options[:bucket], 
	options[:service], 
	options[:region], 
	options[:max_keys], 
	options[:prefix]
)

url = URI.parse(uri)

proxy_addr = 'localhost'
proxy_port = 8081

http = Net::HTTP.new(url.host, url.port) #, proxy_addr, proxy_port)

http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Get.new(url.request_uri)

s3req = sprintf("GET\n/\n%4$s\nhost:%1$s\nx-amz-content-sha256:%2$s\nx-amz-date:%3$s\n\nhost;x-amz-content-sha256;x-amz-date\n%2$s", url.host, req_sha, curr_time, url.query )

req_digest = Digest::SHA256.hexdigest(s3req)

signature_key = getSignatureKey( options[:secret_access_key], curr_date, options[:region], options[:service] )

data = sprintf("AWS4-HMAC-SHA256\n%s\n%s/%s/%s/aws4_request\n%s", curr_time, curr_date, options[:region], options[:service], req_digest)

signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), signature_key, data)

auth_header = sprintf(
	"AWS4-HMAC-SHA256 Credential=%s/%s/%s/%s/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date, Signature=%s", 
	options[:access_key], 
	curr_date, 
	options[:region], 
	options[:service],
	signature)

request["host"] = url.host
request["x-amz-content-sha256"] = req_sha
request["x-amz-date"] = curr_time
request["Authorization"] = auth_header
request['User-Agent'] = "Mozilla/5.0 (Log Collector)"
response = http.request(request)

printf("response code: %s\n", response.code)
doc = Nokogiri::XML(response.body)

doc.css('Contents').each do |item| 
	printf("%s %s\t%s\n", item.at_css('LastModified').content.ljust(26), item.at_css('Key').content, item.at_css('Size').content)
end
