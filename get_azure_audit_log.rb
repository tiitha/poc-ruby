#!/usr/bin/env ruby
require 'net/http'
require 'openssl'
require 'json'


client_id 		= "client-id"
client_secret	= "client-secret"

tenant_domain = "domain.name"
azure_uri			= "https://login.microsoftonline.com/%s/oauth2/token"


uri = URI.parse(sprintf(azure_uri, tenant_domain))
http = Net::HTTP.new(uri.host, uri.port)

http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life

request = Net::HTTP::Post.new(uri)
request.body = sprintf("grant_type=client_credentials&client_id=%s&client_secret=%s&resource=https://graph.microsoft.com/", client_id, client_secret)

response = http.request(request)

token = nil
printf("Token response code: %d\n",response.code)

if response.code == "200"
	token = JSON.parse(response.body)['access_token']
end

raise "Bearer token not defined." if token.nil?

# audit uri w. filter
# filter settings: https://developer.microsoft.com/en-us/graph/docs/api-reference/beta/api/signin_list

audit_uri = "https://graph.microsoft.com/beta/auditLogs/signIns?&$filter=createdDateTime ge 2018-06-08"

uri = URI.parse(audit_uri)

req = Net::HTTP::Get.new(uri)
req["Content-Type"] = "application/json"
req["Authorization"] = sprintf("Bearer %s", token)

http = Net::HTTP.new(uri.host, uri.port)

http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life
response = http.request(req)

printf("payload response code: %d\n",response.code)
result = JSON.parse(response.body)

printf("Got %d lines from Azure Audit logs (SignIns).\n", result['value'].length)

result['value'].each do |l|
	printf("%s\t%s\t%s %s %s %s\t%s\n", 
		l['createdDateTime'], 
		l['ipAddress'], 
		l['deviceDetail']['browser'].ljust(25),
		l['userPrincipalName'].ljust(25), 
		l['userDisplayName'].ljust(25), 
		l['status']['errorCode'],
		l['status']['additionalDetails']
		)
end
